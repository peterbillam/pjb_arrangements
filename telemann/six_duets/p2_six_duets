.\" header = mushead
.AM
.nr PS 14
.nr VS 16
.pl 11.7i
.LP
\&
.sp 1.5c
.LP
.ce 1
\f3\s+7Six Duets, arranged for Recorder\s0\fP
.sp 1.5c
.LP
.nh
Georg Philipp Telemann, the most prolific composer of his time,
and widely regarded as Germany's leading composer 
during the first half of the 18th century, was born in Magdeburg in 1681.
.LP
.nh
In 1727 in Hamburg he self-published these six duets,
(TWV 40:101-106).
The complete title is "\f2Sonates sans basse,
� deux Flutes traversi�res ou � deux Violons ou � deux Flutes � bec,
d�di�es � Messieurs George Behrmann et Pierre Ditric Toennies
par George Philippe Telemann, Directeur de la Musique � Hambourg 1727.\fP"
.LP
.nh
The two dedicatees were keen amateur musicians
from good Hamburg families;
it is interesting to note that Telemann
intended these Duets for educated amateurs.

.LP
.nh
In this edition they have been transposed into the keys in which
modern recorder players would expect to read them.
.TS
center,box;
c c c c c
c c c c l.
Duet	Original	C recorder	F recorder	TWV
_
1.	G maj	F maj	Bb maj	TWV 40:101
2.	D maj	C maj	F maj	TWV 40:102
3.	A maj	G maj	d maj	TWV 40:103
4.	e min	d min	g min	TWV 40:104
5.	b min	a min	d min	TWV 40:105
6.	E maj	D maj	G maj	TWV 40:106
.TE
.LP
.nh
In some editions, for example M�seler, the first two Sonatas are
exchanged. Here I have followed the TWV order.

.LP
.nh
Telemann died of a chest ailment in 1767 in Hamburg.
His grandson Georg Michael inherited a large number of
autographs and manuscript copies of his vocal works.
The rest of his musical estate was sold at auction in Hamburg on the 6th
of September 1769, and most of this material has disappeared.
.sp 0.6c
.LP
.ce 1
\f2Peter J Billam, \ \ www.pjb.com.au\fP


